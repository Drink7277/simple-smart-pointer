//
//  Character.hpp
//  Simple Smart Pointer
//
//  Created by Drink on 13/5/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#ifndef character_testing_hpp
#define character_testing_hpp

#include <stdio.h>

namespace character_testing {

    class Character{

    public:

        static const int DEFAULT_HP;
        static const std::string DEFAULT_NAME;

        Character();
        Character(int t_hp);
        virtual ~Character();
        void print_hp();

        std::string name;

    protected:

        int m_hp;

    };

    class Monster: public Character {

    public:
        Monster();
        Monster(int t_hp);
    };
    class Hero: public Character {

    public:
        Hero();
        Hero(int t_hp);
    };
}

#endif /* Character_hpp */
