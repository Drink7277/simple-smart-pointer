//
//  Character.cpp
//  Simple Smart Pointer
//
//  Created by Drink on 13/5/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#include <iostream>
#include "character.hpp"

using namespace std;

namespace character_testing {
    const int Character::DEFAULT_HP = 100;
    const string Character::DEFAULT_NAME = "Unknown";

    Character::Character() : Character(Character::DEFAULT_HP) {}
    Character::Character(int t_hp) : m_hp(t_hp) , name(Character::DEFAULT_NAME) {}

    Character::~Character() {
        cout << name <<" is killed " << this << endl;
    }

    void Character::print_hp(){
        cout << name << "\'s HP: " << m_hp << endl;
    }


    Monster::Monster() : Character() {}
    Monster::Monster(int t_hp) : Character(t_hp) {}


    Hero::Hero() : Character() {}
    Hero::Hero(int t_hp) : Character(t_hp) {}

}
