//
//  smart_pointer.hpp
//  Simple Smart Pointer
//
//  Created by Drink on 12/5/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#ifndef simple_smart_pointer_hpp
#define simple_smart_pointer_hpp

#include <map>

using namespace std;

namespace simple_smart_pointer {

    template <class T>
    class SimpleSmartPointer{

    public:

        SimpleSmartPointer();
        SimpleSmartPointer(T *t_pointer);
        virtual ~SimpleSmartPointer();

        T & operator * ();
        T * operator -> ();

        static void print_map();

    private:

        T *m_pointer;
        static map<T*, unsigned int> m_pointer_map;

    };

}


#endif
