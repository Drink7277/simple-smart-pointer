//
//  smart_pointer.cpp
//  Simple Smart Pointer
//
//  Created by Drink on 12/5/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#include <iostream>
#include <stdio.h>
#include "smart_pointer.hpp"

using namespace std;

namespace simple_smart_pointer {

    template <class T>
    map<T*, unsigned int> SimpleSmartPointer<T>::m_pointer_map;

    template <class T>
    SimpleSmartPointer<T>::SimpleSmartPointer() : SimpleSmartPointer(new T()) {}

    template <class T>
    SimpleSmartPointer<T>::SimpleSmartPointer(T *t_pointer) : m_pointer(t_pointer) {
        if(m_pointer == nullptr){
            throw "SimpleSmartPointer does not support nullptr";
        }

        // insert will not change the value if key is exist
        m_pointer_map.insert(pair<T*, unsigned int>(t_pointer, 0));
        m_pointer_map[(t_pointer)]++;

    }

    template <class T>
    SimpleSmartPointer<T>::~SimpleSmartPointer(){
        m_pointer_map[m_pointer]--;

        if(m_pointer_map[m_pointer] == 0){
            cout << "delete: " << m_pointer << endl;
            m_pointer_map.erase(m_pointer);
            delete(m_pointer);
        }
    }

    template <class T>
    void SimpleSmartPointer<T>::print_map(){
        printf("%15s|%15s\n", "Address", "Number of Pointer");
        printf("---------------+---------------\n");
        for(auto itr = m_pointer_map.begin(); itr != m_pointer_map.end(); itr++){
            printf("%15p|%15d\n", itr->first, itr->second);
        }
        printf("---------------+---------------\n\n");
    }

    template <class T>
    T & SimpleSmartPointer<T>::operator*(){
        return *m_pointer;
    }

    template <class T>
    T * SimpleSmartPointer<T>::operator->(){
        return m_pointer;
    }

}
