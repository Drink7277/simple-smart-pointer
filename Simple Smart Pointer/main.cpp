//
//  main.cpp
//  Simple Smart Pointer
//
//  Created by Drink on 12/5/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#include <iostream>
#include "smart_pointer.cpp"
#include "character.hpp"


using namespace std;
using namespace simple_smart_pointer;
using namespace character_testing;

int main(int argc, const char * argv[]) {

    cout << "===== START =====" << endl << endl;
    {
        // This will throw error due to prevent nullptr to store in smart pointer
        // SimpleSmartPointer<Character> character_pointer(nullptr);

        // create one monster (2 smart pointers)
        Monster *monster_pointer = new Monster(20);
        SimpleSmartPointer<Monster> monster_simple_pointer1(monster_pointer);
        SimpleSmartPointer<Monster> monster_simple_pointer2(monster_pointer);
        monster_simple_pointer1->name = "Dog";
        monster_simple_pointer2->print_hp();

        // create one hero (2 smart pointers)
        Hero *hero_pointer = new Hero(50);
        SimpleSmartPointer<Hero> hero_simple_pointer1(hero_pointer);
        SimpleSmartPointer<Hero> hero_simple_pointer2(hero_pointer);
        hero_simple_pointer1->name = "Knight";
        hero_simple_pointer2->print_hp();

        // create with default constructor
        SimpleSmartPointer<Hero> default_hero_pointer;
        default_hero_pointer->name = "Default Hero";
        SimpleSmartPointer<Monster> default_monster_pointer;
        default_monster_pointer->name = "Default Monster";

        cout << "Monster--------+" << endl;
        SimpleSmartPointer<Monster>::print_map();

        cout << "Hero-----------+" << endl;
        SimpleSmartPointer<Hero>::print_map();

    }
    cout << "===== END =====" << endl;

    return 0;
}
